# PowerShell

### List ports in use (with text search)

```
netstat -ano | select-string "10.0.0.1"
```

### Change time zone

```
Set-TimeZone -Id "Russian Standard Time"
```

### Active Directory ― Show Replication Status

```
repadmin /showrepl
```

### Active Directory ― Show FSMO Roles

```
netdom query fsmo
```
